# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import device


def register():
    Pool.register(
        device.DeviceType,
        device.Device,
        device.DeviceRequest,
        module='device', type_='model')
