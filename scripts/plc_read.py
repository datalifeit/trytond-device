import argparse
import xmlrpclib
import serial
import base64
import os
import sys
import subprocess

HOST = '<HOST>'
PORT = '<PORT>'
DB = '<DB>'
USER = '<USER>'
PASSWORD = '<PASSWORD>'

DEVICE_CODE = '<DEVICE_CODE>'

COM_PORT = '<COM_PORT>'
TIMEOUT = 1
BAUD = 9600
BYTESIZE = 7
STOPBITS = 2
PARITY = serial.PARITY_EVEN
RTSCTS = False
READ_SIZE = 100


def calculate_fcs(value):
    res = 0  
    for char in value: 
        res ^= ord(char)
    return str(res / 16) + str(res % 16)


def create_command(node, command, params):
    assert(len(node) == 2)
    assert(len(command) == 2)
    assert(len(params) < 124)
    frame = '@' + node + command + params
    return frame + calculate_fcs(frame) + '*\r'


class Tryton(object):

    def __init__(self):
        # Get user_id and session
        self.server = xmlrpclib.ServerProxy('http://%s:%s@%s:%s/%s/' % (
            USER, PASSWORD, HOST, PORT, DB), allow_none=True)

        # Get the user context
        self.pref = self.server.model.res.user.get_preferences(True, {})

    def execute(self, method, *args):
        args += (self.pref,)
        return getattr(self.server, method)(*args)


def parse_commandline():
    parser = argparse.ArgumentParser(prog='register_request')
    parser.add_argument("-d", "--debug", dest="debug", action="store_true",
        help="specify if run in debug mode")
    parser.add_argument("-dev", "--device", dest="device",
        help="specify if run in debug mode")
    parser.add_argument("-c", "--command", dest="command",
        nargs='+', default=[], help="specify if run in debug mode")
    parser.add_argument("-n", "--node", dest="node",
        nargs='+', default=[], help="specify if run in debug mode")
    parser.add_argument("-p", "--params", dest="params",
        nargs='+', default=[], help="specify if run in debug mode")

    options = parser.parse_args()
    return options


if __name__ == '__main__':
    options = parse_commandline()

    a = Tryton()

    if not options.debug:
        ser = serial.Serial(COM_PORT, BAUD, timeout=TIMEOUT,
            bytesize=BYTESIZE, stopbits=STOPBITS, parity=PARITY, rtscts=RTSCTS)

    ii = 0
    while True:
        try:
            node = options.node[0] if options.node else '00'
            command_type = options.command[0] if options.command else 'RD'
            params = options.params[0] if options.params else '00000004'
            request = create_command(node, command_type, params)
            if options.debug:
                response = create_command('00', 'RD', '001230481730914A')
            else:
                ser.write(request)
                response = ser.read(size=READ_SIZE)
            if response:
                message = {
                    'device_code': options.device if options.device else DEVICE_CODE,
                    'request': request,
                    'response': response
                }
                print('Sending {0}'.format(message))
                a.execute(
                    'model.device.request.register_request', [message])
        except StandardError:
            e = sys.exc_info()[0]
            print(sys.exc_info()[1])
        finally:
            if options.debug:
                break
